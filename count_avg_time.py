#!/usr/bin/ python
# -*- coding: utf-8 -*-

import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f')

    args = parser.parse_args()

    logs = []
    with open(args.f, 'r') as log_file:
        for line in log_file:
            logs.append(line.rstrip("\n").split())

    batch_size = logs[1][0]
    iteration_time = 0
    total_time = 0.0
    digit = [str(i + 1)for i in range(9)]
    for line in logs:
        if line[0][-1] == ':' and line[0][0] in digit:
            iteration_time += 1
            total_time += float(line[6])

    avg_time = total_time / iteration_time
    print('When batch size = {}, avg. time = {}\n'.format(batch_size, avg_time))


if __name__ == "__main__":
    main()
