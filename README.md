# High-Performance Computing and Big Data Systems Final Project

2018 Fall National Taiwan University

--------------------------------------------------------------------------------

## YOLO with Different Batch Size

### Introduction

You only look once (YOLO) is a state-of-the-art, real-time object detection system. In this project, we use yolo to achieve barcode and QR code detection with different batch size to find out how batch size influence the training efficiency and the performance of YOLO.

### Training

1. Modify darknet/cfg/yolo.obj.cfg

     1. change line classes = to your number of objects
     2. change line #224 : filters=(classes + 5)*5

2. Create file obj.names in directory darknet/data, with objects names each in new line

3. Create file obj.data in the directory darknet/data, example:

        classes = 2 # barcode and qrcode
        train = data/train.txt # where you place your training images
        valid = data/test.txt
        names = data/obj.names
        backup = backup # backup is a directory, where you want to save your weights

4. Create .txt-file for each .jpg-image-file in the same directory and with the same name, but with .txt-extension, and put to file: object number and object coordinates on this image, for each object in new line:

        <object-class> <x> <y> <width> <height>

5. Create file train.txt in directory , with filenames of your images, each filename in new line, for example train.txt containing:

        labels/img1.jpg
        labels/img2.jpg
        labels/img3.jpg

6. Start training by using the command line:

        $ ./darknet detector train data/obj.data cfg/yolo.obj.cfg darknet19_448.conv.23

