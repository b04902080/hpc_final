#!/usr/bin/ python
# -*- coding: utf-8 -*-


def main():
    txt_list = []
    with open('./list.txt', 'r') as file:
        for line in file:
            txt_list.append(line.rstrip("\n"))

    total_img = 0
    qr_code = 0
    barcode = 0
    for line in txt_list:
        with open(line, 'r') as img_log:
            total_img += 1
            for log in img_log:
                if log[0] == 'c':
                    barcode += 1
                elif log[0] == 'v':
                    qr_code += 1

    print('# of images = {}\n# of barcode = {}\n# of QR code = {}\n'.format(
        total_img, barcode, qr_code))


if __name__ == "__main__":
    main()
